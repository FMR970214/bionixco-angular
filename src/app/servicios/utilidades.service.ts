import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import * as moment from 'moment';
const headers = { headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': 'my-auth-token', 'Accept': 'application/json', }) };
@Injectable({
    providedIn: 'root'
})
export class UtilidadesService {
    constructor(private http: HttpClient,private sanitizer:DomSanitizer) { }
    public GET(peticion:string = "/"):Observable<any>{
        return this.http.get<any>(peticion);
    }
    public POST(peticion:string = "/",postData:any = {},header:any = headers):Observable<any>{
        if(postData instanceof FormData){
            return this.http.post<any>(peticion, postData);
        }else{
            return this.http.post<any>(peticion, postData,header);
        }
    }
    public isNumeric($obj:any, $strict:boolean = false):boolean{
        if ($strict) {
            return !isNaN($obj) && $obj instanceof Number ? true : false;
        } else {
            return !isNaN($obj - parseFloat($obj));
        }
    }
    public JsonSetting($var:any):any{
        return JSON.parse(JSON.stringify($var));
    }
    public compararFechas($fechaI:string,$fechaF:string):any{
        if($fechaI != null && $fechaI != '' && $fechaF != null && $fechaF != ''){
            if(moment($fechaF).diff($fechaI, 'days') > 0){
                return {min : $fechaI, max : $fechaF};
            }else{
                return {min : $fechaF, max : $fechaI};
            }
        }else if($fechaI != null && $fechaI != ''){
            return {min : $fechaI, max : null};
        }else if( $fechaF != null && $fechaF != ''){
            return {min: $fechaF, max : null}
        }else{
            return {min:null,max:null};
        }
    }
    public calcScrollTable($id:string, $reduce:number = 0, $default:string = '200px'):string | null{
        if(document.getElementById($id) != null){
            //console.log((document.getElementById($id).offsetHeight - $reduce) + 'px');
            return (document.getElementById($id)!.offsetHeight - $reduce) + 'px';
        }else{
            return $default;
        }
    }
    public sortByField(property:string) {
        let sortOrder = 1;
        if(property[0] === "-") {
            sortOrder = -1;
            property = property.substring(1);
        }
        return function (a: { [x: string]: number; },b: { [x: string]: number; }) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        }
    }
    public formatHtmlDate($date:string):string|null{
        //retorna la fecha para formato input html
        if($date != null){
            return ($date.replace(' ','T')).substring(0,19);
        }else{
            return null;
        }
    }
    public codigoAleatorio($length:number):string{
        let recurso="0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        let codigo='';
        for (let index = 0; index < $length; index++) {
            codigo+=recurso.substr(Math.random()*recurso.length-1,1);
        }
        return codigo;
    }
    //calcula el digito de verificacion de los documentos
    public calcDV($documento:string|number){
        if(typeof $documento == 'number'){
            $documento=$documento.toString();
        }
        let calculos:any=[];
        let sumDocumento:number=0;
        let exponentes=[3,7,13,17,19,23,29,37,41,43,47,53,59,67,71];
        let documento=$documento.padStart(15,'0');
        for (let x=(documento.length-1),e=0; x >= 0; x--,e++) {
            calculos.push(parseInt(documento[x])*exponentes[e]);
        }
        let residuo=calculos.reduce((a:number, b:number) => a + b, 0)%11;
        if(residuo==0 || residuo==1){
            return residuo;
        }else{
            return 11-residuo;
        }
    }
    public sanitizeUrl($url:string):any{
        return this.sanitizer.bypassSecurityTrustUrl($url);
    }
    public sumArray($data:any,$key=null):number{
        let sum=0;
        if($key!=null){
            $data.forEach((row: { [x: string]: any; }) => {
                let value = row[$key]!=null && row[$key]!=undefined ? row[$key] : 0;
                sum += value;
            });
        }else{
            $data.forEach((row: number) => {
                sum+=row;
            });
        }
        return sum;
    }
    public calcPorcentaje($100:any, $x:any, $retorno:string = 'string'):any{
        let porcentaje:string='';
        if($x != '' && $100 != ''){
            porcentaje = ((100 * $x) / $100).toFixed(1).toString();
        }
        if(porcentaje != ''){
            if($retorno == 'number'){
                return porcentaje;
            }else{
                return porcentaje+'%';
            }
        }else{
            if($retorno == 'number'){
                return 0;
            }else{
                return '0%';
            }
        }  
    }
    public getPorcentaje($100:number, $x:number, $decimales:number = 0, $default:number|string = 0):number|string{
        let porcentaje:number = 0;
        porcentaje = parseFloat(((100 * $x) / $100).toFixed($decimales))
        if(Number.isNaN(porcentaje)){
            return $default;
        }else{
            return porcentaje;
        }
        
    }
    //toma la data de un formulario y envia solo los valores necesarios de un cojunto de llaves
    public prepareServerFormData($data:any, $renombrar:any = null){
        let keys = Object.keys($data)
        let formData = new FormData();
        keys.forEach(key => {
            if($renombrar != null && $renombrar[key] != undefined){
                formData.append($renombrar[key],$data[key]);
            }else{
                formData.append(key,$data[key]);
            }
        });
        return formData;
    }
    /*public prepareServerFormData($data:any, $keys:string[], $lowerExceptions:string[] = []){
        let formData = new FormData();
        $keys.forEach(key => {
            if(typeof $data[key] == "string"){
                let indexException = $lowerExceptions.findIndex(exception => {
                    return exception === key;
                });
                if(indexException == -1){
                    $data[key]=$data[key].toLowerCase();
                }else{
                    $data[key]=$data[key]
                }
            }
            if($data[key]==undefined){
                formData.append(key, null);
                //formData[key]=null;
            }else{
                formData.append(key,$data[key]);
                //formData[key]=$data[key];
            }
        });
        return formData;
    }*/
    /*public prepareServerData($data:any, $keys:string[], $lowerExceptions:string[] = []){
        let formData:any = {}
        $keys.forEach(key => {
            if(typeof $data[key] == "string"){
                let indexException = $lowerExceptions.findIndex(exception => {
                    return exception === key;
                });
                if(indexException == -1){
                    $data[key] = $data[key].toLowerCase();
                }else{
                    $data[key] = $data[key];
                }
            }
            if($data[key] == undefined){
                formData[key] = null;
            }else{
                formData[key] = $data[key];
            }
        });
        return formData;
    }*/
    public imprimirFormData($formData:any):any{
        let data:any = [];
        for (var row of $formData.entries()) {
            console.log(row);
            data[row[0]] = data[row[1]];
        }
        return data;
    }
    public calcHeightRows($columns:any, $data:any, $default:number):string{
        let tabs:number = 0;
        $columns.forEach((col: { width: number; field: string | number; }) => {
            if(col.width > 0){
                let maxWidth:number =col.width / 5.5;//4.65 es el tamaño promedio de una letra en px.
                try {
                    //console.log($data[col.field].length, maxWidth, Math.floor($data[col.field].length / maxWidth));
                    let multiplicador:number = Math.floor($data[col.field].length / maxWidth);
                    if(multiplicador > tabs){
                        tabs = multiplicador;
                    }
                } catch (error) {
                    //aqui entrará si el valor de $data[col.field] es null o undefined
                } 
            }
        });
        //17.6 es el valor que se le suma al salto de linea de una celda.
        return ($default + (17.6 * tabs)) + 'px';
    }
    public sepMiles:string = '.';
    public sepDecimal:string = ',';
    public formatNumber(num:any|null, simbol:string|null = null):any{
        if(num == null || num == undefined){
            return 0;
        }
        num += '';
        let splitStr = num.split('.');
        let splitLeft = splitStr[0];
        let splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
        let regx = /(\d+)(\d{3})/;
        while (regx.test(splitLeft)) {
            splitLeft = splitLeft.replace(regx, '$1' + this.sepMiles + '$2');
        }
        return simbol + splitLeft +splitRight;
    }
}
