import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Prueba1Component } from './prueba1.component';
import {TableModule} from 'primeng/table';
import {DropdownModule} from 'primeng/dropdown';
import {MatSelectModule} from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [Prueba1Component],
  imports: [
    CommonModule,
    TableModule,
    DropdownModule,
    MatSelectModule,
    MatInputModule,
    ButtonModule,
    FormsModule
  ]
})
export class Prueba1Module { }
