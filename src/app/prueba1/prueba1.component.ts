import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-prueba1',
  templateUrl: './prueba1.component.html',
  styleUrls: ['./prueba1.component.scss']
})
export class Prueba1Component implements OnInit {

  constructor(private http:HttpClient) { }
  public dataHombres:any = [];
  public dataMujeres:any = [];
  public globalData:any = [];
  public registros:number = 100
  public optRegistros:any = [
    {label:1000, value:1000},
    {label:100, value:100},
    {label:75, value:75},
    {label:50, value:50},
    {label:25, value:25},
    {label:10, value:10}
  ]
  ngOnInit(): void {
    this.getDataRest();
  }
  public getDataRest():void{
    console.log('obteniendo datos')
    this.http.get<any>('https://randomuser.me/api/?results='+this.registros).subscribe(rta => {
      console.log(rta)
      this.globalData = rta.results
      console.log(this.globalData)
      this.dataHombres = this.globalData.filter( person =>{
        return person.gender == 'male'
      });
      this.dataHombres.sort((a,b) => {
        a.fullname = a.name.title + ' ' + a.name.first + ' ' + a.name.last
        b.fullname = b.name.title + ' ' + b.name.first + ' ' + b.name.last
        return a.dob.age - b.dob.age
      });
      this.dataMujeres = this.globalData.filter(person=>{
        return person.gender == 'female'
      });
      this.dataMujeres.sort((a, b) => {
        a.fullname = a.name.title + ' ' + a.name.first + ' ' + a.name.last
        b.fullname = b.name.title + ' ' + b.name.first + ' ' + b.name.last
        if(a.email.toLowerCase() < b.email.toLowerCase()) { 
          return 1;
        }
        if(a.email.toLowerCase() > b.email.toLowerCase()) { 
          return -1;
        }
        return 0;
      });
      console.log(this.dataHombres)
      console.log(this.dataMujeres)

    },error => {
      console.log(error)
    })
  }

}
