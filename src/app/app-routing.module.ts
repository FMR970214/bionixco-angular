import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Prueba1Component } from './prueba1/prueba1.component';

const routes: Routes = [
  {path:'', component: Prueba1Component},
  {path:'prueba1', component: Prueba1Component}
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
